from setuptools import setup
setup(
    name='wpdownloadercli',
    version='0.1.0',
    packages=['wpdownloadercli'],
    entry_points={
        'console_scripts': [
            'wpdownloadercli = wpdownloadercli.__main__:main'
        ]
    })