import logging
import sys

LOGGING_FORMAT = "%(asctime)s %(levelname)8s [%(name)s] %(funcName)s:%(lineno)-4s %(message)s"


def setup_logger(name, level=logging.DEBUG):
    """Function setup as many loggers as you want"""

    formatter = logging.Formatter(fmt=LOGGING_FORMAT, datefmt="%Y-%m-%d %H:%M:%S")
    handler = logging.StreamHandler(sys.stderr)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger
