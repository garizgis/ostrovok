import os

import wget

from wpdownloadercli.logger import setup_logger
from wpdownloadercli.url_parser import simple_get

log = setup_logger('downloader')


def download_wallpapers(links_list, out_dir):
    for link in links_list:
        path = os.path.join(out_dir, link.split('/')[-1])
        response = simple_get(link)
        if response is not None:
            if not os.path.exists(os.path.dirname(path)):
                os.makedirs(os.path.dirname(path))
            if not os.path.exists(path):
                wget.download(link, out=os.path.dirname(path))
