import inspect
import logging
import re
from datetime import datetime

from dateutil.relativedelta import relativedelta
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup

from wpdownloadercli.logger import setup_logger

root_url = 'https://www.smashingmagazine.com'
log = setup_logger('url_parser')


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            a = is_good_response(resp)
            if a:
                if inspect.stack()[1].function == 'download_wallpapers':
                    return resp.raw
                else:
                    return resp.text
            else:
                log.error(f'Error during requests to {url} : {resp.status_code}')
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None)
            # and content_type.find('html') > -1)


def log_error(e):
    """
    It is always a good idea to log errors.
    This function just prints them, but you can
    make it do anything.
    """
    print(e)


def page_gen():
    a = 1
    while True:
        yield a
        a += 1


def get_wallpapers_links(resolution, time_period, extension):
    """
    Downloads the page where the list of mathematicians is found
    and returns a list of strings, one per mathematician
    """
    links_list = []
    time_period = datetime(int(time_period.split('/')[0]),
                           int(time_period.split('/')[1]), 1)
    time_period = time_period + relativedelta(months=-1)
    time_period = time_period.strftime('%Y/%m')
    for page in page_gen():
        if page == 1:
            url = f'{root_url}/category/wallpapers/'
        else:
            url = f'{root_url}/category/wallpapers/page/{page}'
        response = simple_get(url)
        if response is not None:
            soap = BeautifulSoup(response, 'html.parser')
            url_list = soap.find_all('a')
            for url in url_list:
                link = re.search(time_period, str(url))
                if link:
                    next_url = root_url + url['href']
                    next_response = simple_get(next_url)
                    if next_response is not None:
                        next_soap = BeautifulSoup(next_response, 'html.parser')
                        next_url_list = next_soap.find_all('a')
                        for next_url in next_url_list:
                            link = re.search(f'{resolution}.{extension}',
                                             str(next_url))
                            if link:
                                links_list.append(next_url['href'])
        else:
            break
        #     raise Exception('Error retrieving contents at {}'.format(url))
    return links_list
    # Raise an exception if we failed to get any data from the url
    #
