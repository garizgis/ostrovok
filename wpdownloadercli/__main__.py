import argparse
import logging
import os
import shutil
import sys

from wpdownloadercli.downloader import download_wallpapers
from wpdownloadercli.logger import setup_logger
from wpdownloadercli.url_parser import get_wallpapers_links


def main():

    log = setup_logger('main')

    def create_parser():
        parser = argparse.ArgumentParser(
            description='Command line wallpapers downloader',
            usage='pass resolution and time period to download wallpapers'
        )
        parser.add_argument(
            'resolution',
            choices=[
                '320x480', '640x480', '800x480', '800x600', '1024x768',
                '1024x1024', '1152x864', '1280x720', '1280x960', '1280x1024',
                '1366x768', '1400x1050', '1440x900', '1600x1200', '1680x1050',
                '1680x1200', '1920x1080', '1920x1200', '1920x1440',
                '2560x1440'
            ],
            help='resolution of the selected wallpapers set'
        )
        parser.add_argument(
            'time_period',
            default='2017/05',
            help='time period of the selected wallpapers set, e.g. 2017/05'
        )
        parser.add_argument(
            '--extension',
            default='jpg',
            choices=['jpg', 'png'],
            help='wallpapers filename extension'
        )
        parser.add_argument(
             '--output_dir',
             default=os.path.join(os.path.abspath(os.path.curdir), 'wallpapers'),
             help='path to storage root directory, current directory by default'
        )
        return parser

    agr_parser = create_parser()
    args = agr_parser.parse_args()
    links_list = get_wallpapers_links(args.resolution, args.time_period,
                                      args.extension)
    output_dir = os.path.join(args.output_dir, args.time_period)
    log.info(f'Find {len(links_list)} wallpapers')
    download_wallpapers(links_list, output_dir)


if __name__ == '__main__':
    main()
